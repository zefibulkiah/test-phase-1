<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.82.0">
    <title>Test Phase 1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/heroes/">

    

    <!-- Bootstrap core CSS -->
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="heroes.css" rel="stylesheet">
  </head>
  <body>

<?php
$daysInWeek = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");//array

$todayIs = date('l');//what day is today

?>
    
<div class="px-4 py-5 my-5 text-center">
  <h1 class="display-5 fw-bold">Developer Test Phase 1</h1>
  <div class="col-lg-6 mx-auto">
    <p class="lead mb-4">todays is <?php echo $todayIs; ?></p>
    <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
      <?php
      //start looping
      foreach ($daysInWeek as $key => $value) {
      // for each $daysInWeek as $key in $value
      // or you can say loop the array an get the data 

        //check the data
        if($value==$todayIs){
          // if $value or the data is the sama as today
          // do this
          $btnStyl="btn-primary";
        }else{
          // if not the same (else)
          // do this
          $btnStyl="btn-outline-secondary";
        }
        
        echo '<a href="?Select='.$value.'" class="btn '.$btnStyl.' btn-lg px-4 px-4">'.$value.'</a>';
      }
      ?>
    </div>
    <p class="lead mb-4">Selected day is 
      <?php 
        if(isset($_GET['Select'])){ 
          echo $_GET['Select'];
        }
      ?>
    </p>
  </div>
</div>

<!--


<div class="b-example-divider mb-0"></div>

-->
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>

      
  </body>
</html>
